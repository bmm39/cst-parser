const moment = require("moment");
const puppeteer = require("puppeteer");
const log4js = require('log4js');
const mysql = require("mysql2/promise");

const mailer = require('../utils/mailer');
const C = require("./config");

const logger = log4js.getLogger();
logger.level = 'debug';

const getSlotType = configType => {
  return {
    SLOT: "СЛОТЫ",
    SLOT_RDS: "РДС",
    SLOTARX: "АРХИВ"
  }[configType];
};

const formatDayNumberFormat = id => {
  return {
    1: "1",
    2: "2",
    3: "3",
    4: "4",
    5: "5",
    6: "6",
    0: "7"
  }[id];
};

const getConfig = async () => {
  let result = null;
  logger.debug("START GETTING CONFIG FROM DB");
  try {
    const connection = await mysql.createConnection(C.DB);

    const [rows] = await connection.query("SELECT `data` FROM `config`;");
    result = JSON.parse(rows[0].data);
    logger.debug("END GETTING CONFIG FROM DB WITHOUT ERROR");
  } catch (e) {
    logger.debug("GETTING CONFIG ERROR:", e);
  }
  return result;
};

const sleep = time => new Promise(resolve => setTimeout(resolve, time));

const writeDataToDB = async data => {
  logger.debug("START WRITING DATA TO DB");
  try {
    const connection = await mysql.createConnection(C.DB);

    for (const slot of data) {
      await connection.execute(
        "INSERT INTO `slots` (`season`, `departure_airport`, `dep_terminal`, `slot`, `dep_arr`, `carrier`, `flight_num`," +
        " `flight_date`, `flight_day`, `AC_type`, `AC_cat`, `AC_CAP`, `RDS`, `flight_type`, `destination_city`, " +
        "`intermediate_city`, `origin_city`, `service_type`, `country`, `type`, `report_id`) " +
        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
        [
          slot.season,
          slot.departure_airport,
          slot.dep_terminal,
          slot.slot,
          slot.dep_arr,
          slot.carrier,
          slot.flight_num,
          slot.flight_date,
          slot.flight_day,
          slot.AC_type,
          slot.AC_cat,
          slot.AC_CAP,
          slot.RDC,
          slot.flight_type,
          slot.destination_city,
          slot.intermediate_city,
          slot.origin_city,
          slot.service_type,
          slot.country,
          slot.type,
          slot.reportID,
        ]
      );
    };
    logger.debug("END WRITING DATA TO DB WITHOUT ERROR");
  } catch (e) {
    logger.debug("WRITING DATA TO DB ERROR:", e);
  }
};

const scrapingData = async (city, config, slot, reportID) => {
  logger.debug("SLOT", slot);
  logger.debug("CITY", city);
  logger.debug("\nCONFIG:\n", config);
  const browser = await puppeteer.launch({
    headless: true
  });

  try {
    const type = getSlotType(slot.name);
    const page = await browser.newPage();
    await page.goto(config.URL, {
      waitUntil: "networkidle2"
    });
    await page.type(config.username.selector, config.username.value);
    await page.type(config.password.selector, config.password.value);
    await page.click(C.submitAuthS);
    await sleep(3000);
    try {
      await page.waitForSelector(C.slotsTabelButtonS);
      await page.click(C.slotsTabelButtonS);
    } catch (error) {
      const data = await page.evaluate(() => {
        return document.querySelector("#pageview > form:nth-child(1) > table > tbody > tr:nth-child(4) > td").innerText;
      });
      logger.debug("AUTH ERROR \n", data);
      mailer.sendEmail(data, reportID);
      await browser.close();
    }
    await page.waitForSelector(config.transportType.selector);
    await page.type(config.transportType.selector, config.transportType.value);
    await sleep(3000);
    await page.type(config.cityS, city);

    if (config.nextPreviousAirports.value.length > 0) {
      await page.type(config.nextPreviousAirports.selector, config.nextPreviousAirports.value);
    }

    await page.select(config.timeMode.selector, config.timeMode.value);

    if(slot.name !== "SLOT") {
      page.select(C.slotTypeS, slot.name);
      await page.waitForNavigation();
      await sleep(3000)
    }

    logger.debug("SLOT SEASON", slot.season);

    if (slot.season != "") {
      page.select(C.seasonS, slot.season);
      await page.waitForNavigation();
      await sleep(3000)
    }

    await page.waitForSelector(C.submitSlotsButtonS);
    await page.click(C.submitSlotsButtonS);
    logger.debug("WAITING DATA LOADING");
    const now = moment();
    await page.waitForNavigation({
      timeout: 20000000,
      waitUntil: "domcontentloaded"
    });
    const then = moment();
    const duration = moment(then - now).format("mm:ss");
    logger.debug("DATA WAS LOADED! DURATION:", `${duration}(mm:ss) ${slot.name}`);

    const data = await page.evaluate(() => {
      const trs = Array.from(
        document.querySelectorAll("table:nth-child(3) > tbody > tr")
      );
      const data2 = trs.map(tr => {
        const array = tr.innerText.split("\t");
        return {
          season: array[0],
          departure_airport: array[1],
          dep_terminal: array[2],
          slot: array[3],
          dep_arr: array[4],
          carrier: array[5].split("-")[0],
          flight_num: array[5].split("-")[1],
          navigation: array[6],
          AC_type: array[7],
          AC_cat: array[8],
          AC_CAP: array[9],
          RDC: array[10],
          flight_type: array[11],
          destination_city: array[12],
          intermediate_city: array[13],
          country: array[14],
          origin_city: array[15],
          service_type: array[12] === array[13] ? "Direct" : "Transit",
        };
      });

      data2.splice(0, 1);
      data2.splice(-1, 1);
      return data2;
    });

    const formatedData = [];

    data.forEach((item, index) => {
      if (item.departure_airport === "") {
        formatedData.push({
          ...formatedData[index - 1],
          navigation: item.season,
          arrayOfDayNumber: item.season
            .split(" ")[0]
            .split("")
            .filter(a => !isNaN(a)),
          navFromDate: item.season.split(" ")[1].split("-")[0],
          navToDate: item.season.split(" ")[1].split("-")[1],
          type: type,
        });
      } else {
        formatedData.push({
          ...item,
          arrayOfDayNumber: item.navigation
            .split(" ")[0]
            .split("")
            .filter(a => !isNaN(a)),
          navFromDate: item.navigation.split(" ")[1].split("-")[0],
          navToDate: item.navigation.split(" ")[1].split("-")[1],
          type: type,
        });
      }
    });

    const formatedData2 = [];

    formatedData.forEach(item => {
      if (item.navFromDate !== item.navToDate) {
        let startDate = moment(item.navFromDate, "DD.MM.YY").format(
          "DD.MM.YYYY"
        );
        while (
          moment(item.navToDate, "DD.MM.YY").diff(
            moment(startDate, "DD.MM.YYYY"),
            "days"
          ) !== -1
          ) {
          const numberOfDay = moment(startDate, "DD.MM.YYYY").day();
          const tableFormat = formatDayNumberFormat(numberOfDay);
          if (item.arrayOfDayNumber.includes(tableFormat)) {
            formatedData2.push({
              ...item,
              reportID,
              flight_date: startDate,
              flight_day: formatDayNumberFormat(numberOfDay)
            });

            startDate = moment(startDate, "DD.MM.YYYY")
              .add(1, "day")
              .format("DD.MM.YYYY");
          } else {
            startDate = moment(startDate, "DD.MM.YYYY")
              .add(1, "day")
              .format("DD.MM.YYYY");
          }
        }
      } else {
        formatedData2.push({
          ...item,
          reportID,
          flight_date: moment(item.navFromDate, "DD.MM.YY").format(
            "DD.MM.YYYY"
          ),
          flight_day: item.arrayOfDayNumber[0]
        });
      }
    });

    logger.debug("ROWS IN TABLE:", data.length);
    logger.debug("ROWS TO DB:", formatedData2.length);

    await browser.close();

    return formatedData2;

  } catch (error) {
    logger.debug("ERROR DATA SCRAPPING", error);
    mailer.sendEmail(error.message, reportID);
    await browser.close();

    return false;
  }
};


const getReportID = async (config) => {
  let result = null;
  logger.debug("START WRITING REPORT DATA TO DB");
  try {
    const connection = await mysql.createConnection(C.DB);

    await connection.query("INSERT INTO `reports` (`config`) VALUES (?);", [config]);
    const [rows] = await connection.query("SELECT MAX(`id`) as id from `reports`;");
    result = rows[0].id;
    logger.debug("END WRITING REPORT DATA TO DB WITHOUT ERROR. REPORT ID:", result);
  } catch (e) {
    logger.debug("WRITING REPORT DATA TO DB ERROR:", e);
  }
  return result;
};

const updateEndReportTime = async (end_date, reportID) => {
  let result = null;
  logger.debug("START UPDATING REPORT DATA");
  try {
    const connection = await mysql.createConnection(C.DB);

    await connection.query("UPDATE `reports` SET `end_date` = ? where id = ?;", [end_date, reportID]);
    logger.debug("END UPDATING REPORT DATA WITHOUT ERROR. REPORT ID:", result);
  } catch (e) {
    logger.debug("UPDATING REPORT DATA ERROR:", e);
  }
  return result;
}


(async () => {
  logger.debug("START");
  const startTime = moment();
  const config = await getConfig();
  const reportID = await getReportID(JSON.stringify(config));
  logger.debug("REPORT ID:", reportID);
  const { cities, slotsConfig } = config;

  for (const slot of slotsConfig) {
    for (const city of cities) {
      logger.debug("GRUB DATA FOR CITY:", city);
      try{
        const scrapedData = await scrapingData(city, config, slot, reportID);
        logger.debug(scrapedData[0]);

        if (scrapedData) {
          await writeDataToDB(scrapedData);
        }
      } catch (error) {
        mailer.sendEmail(error.message, reportID);
        logger.debug('ERROR', error);
      }
    }
  }

  const endTime = moment();
  await updateEndReportTime(endTime.format("YYYY-MM-DD HH:mm:ss"), reportID);
  const duration = moment(endTime - startTime).format("HH:mm:ss");
  mailer.sendEmail('Success', reportID);
  logger.debug("EXECUTION TIME:", duration);
  logger.debug("END");
})();
