var log4js = require('log4js');
var mysql = require("mysql2/promise");

var C = require("../parser/config");

var logger = log4js.getLogger();
logger.level = 'debug';

module.exports = {
  getConfig: async () => {
    let result = null;
    logger.debug("START GETTING CONFIG FROM DB");
    try {
      const connection = await mysql.createConnection(C.DB);

      const [rows] = await connection.query("SELECT `data` FROM `config`;");
      result = JSON.parse(rows[0].data);
      logger.debug('\nCONFIG:', result);
      logger.debug("END GETTING CONFIG FROM DB WITHOUT ERROR");
    } catch (e) {
      logger.debug("GETTING CONFIG ERROR:", e);
    }
    return result;
  },
  saveConfig: async (
    citiesString,
    airport,
    slotArxSeason,
    slotRdsSeason,
    slotSeason,
    username,
    password,
  ) => {
    let result = null;
    logger.debug("START UPDATING CONFIG FROM DB");
    try {
      const connection = await mysql.createConnection(C.DB);

      const [rows] = await connection.query("SELECT `data` FROM `config`;");
      result = JSON.parse(rows[0].data);
      logger.debug(result);
      const newCities = citiesString.split(',');
      const newSlotSeason = { ...result.slotsConfig[0], season: slotSeason };
      const newSlotRdsSeason = { ...result.slotsConfig[1], season: slotRdsSeason };
      const newSlotArxSeason = { ...result.slotsConfig[2], season: slotArxSeason };

      const newConfig = {
        ...result,
        cities: newCities,
        nextPreviousAirports: {
          ...result.nextPreviousAirports,
          value: airport
        },
        username: {
          ...result.username,
          value: username
        },
        password: {
          ...result.password,
          value: password
        },
        slotsConfig: [
          newSlotSeason,
          newSlotRdsSeason,
          newSlotArxSeason
        ]
      };
      logger.debug('\nNEW CONFIG:', newConfig);
      const configForDB = JSON.stringify(newConfig);

      await connection.query("UPDATE `config` SET `data`=?", [configForDB]);
      logger.debug("END UPDATING CONFIG FROM DB WITHOUT ERROR");
    } catch (e) {
      logger.debug("UPDATING CONFIG ERROR:", e);
    }
    return result;
  }
};
