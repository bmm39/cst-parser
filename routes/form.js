var express = require('express');
var router = express.Router();
var config = require('../utils/configDB');
var logger = require('../utils/logger');
var shell = require('shelljs');

/* GET home page. */
router.get('/', async (req, res, next) => {
  try {
    const configData = await config.getConfig();
    res.render('form', {
      slotArxSeason: configData.slotsConfig[2].season,
      slotRdsSeason: configData.slotsConfig[1].season,
      slotSeason: configData.slotsConfig[0].season,
      airport: configData.nextPreviousAirports.value,
      cities: configData.cities.join(),
      username: configData.username.value,
      password: configData.password.value
    } );
  } catch (e) {
    next(e)
  }
});

router.post("/", async function(req, res) {
  logger.debug('\nREQUEST BODY:', req.body);
  const {
    airport,
    cities,
    password,
    slotArxSeason,
    slotRdsSeason,
    slotSeason,
    username,
  } = req.body;
  if (typeof req.body['start'] !== 'undefined') {
    shell.exec('pm2 restart cst-parser');
    res.send(
      "Start parsing"
    );
  } else if (typeof req.body['save'] !== 'undefined') {
    config.saveConfig(
      cities,
      airport,
      slotArxSeason,
      slotRdsSeason,
      slotSeason,
      username,
      password,
    );
    res.send(
      "Config was saved successful"
    );
  } else {
    next();
  }
});

module.exports = router;
